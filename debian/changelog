python-repoze.tm2 (2.2.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Add Rules-Requires-Root: no.
  * Use Testsuite: autopkgtest-pkg-python.

 -- Colin Watson <cjwatson@debian.org>  Tue, 28 May 2024 10:47:22 +0100

python-repoze.tm2 (2.0-3) unstable; urgency=low

  [ Debian Janitor ]
  * Use secure URI in debian/watch.
  * Bump debhelper from deprecated 9 to 12.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Debian Janitor ]
  * Apply multi-arch hints.
    + python-repoze.tm2-doc: Add Multi-Arch: foreign.
  * Bump debhelper from old 12 to 13.

 -- Sandro Tosi <morph@debian.org>  Fri, 03 Jun 2022 23:34:35 -0400

python-repoze.tm2 (2.0-2) unstable; urgency=medium

  * Team upload.

  [ Ondřej Nový ]
  * Fixed VCS URL (https)
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https protocol in Format field
  * d/control: Deprecating priority extra as per policy 4.0.1
  * d/control: Remove ancient X-Python-Version field
  * d/control: Remove ancient X-Python3-Version field
  * Convert git repository from git-dpm to gbp layout
  * Use 'python3 -m sphinx' instead of sphinx-build for building docs
  * Use debhelper-compat instead of debian/compat.

  [ Andrey Rahmatullin ]
  * Drop Python 2 support (Closes: #938133).

 -- Andrey Rahmatullin <wrar@debian.org>  Tue, 24 Sep 2019 12:06:06 +0500

python-repoze.tm2 (2.0-1) unstable; urgency=low

  [ Jakub Wilk ]
  * Use canonical URIs for Vcs-* fields.

  [ Jan Dittberner ]
  * New upstream version
  * update debian/docs to match new upstream files
  * switch to pybuild (Closes: #671329)
  * add Python3 support
  * split into separate Python 2, Python 3 and documentation packages
  * bump debian/compat to 9
  * bump Standards-Version to 3.9.6 (no changes)
  * add extend-diff-ignore for egg-info data in debian/source/options
  * install CHANGES.rst as upstream changelog
  * change debian/copyright to machine readable format
  * add uversionmangle option to debian/watch to sort upstream alpha and
    beta versions correctly

 -- Jan Dittberner <jandd@debian.org>  Sun, 05 Oct 2014 12:14:08 +0200

python-repoze.tm2 (1.0b2-1) unstable; urgency=low

  * New upstream version (Closes: #630704)
  * Adopting package.
  * debian/control:
    - drop cdbs and python-support from Build-Depends
    - bump required debhelper version to (>= 7.0.50~)
    - use python (>= 2.6.5~) instead of python-dev in Build-Depends
    - add version (>= 1.0.7+dfsg) to python-sphinx in Build-Depends
    - bump Standards-Version to 3.9.2 (no changes needed)
    - add ${sphinxdoc:Depends} to Depends
  * debian/rules:
    - use simplified dh rules using --with python2,sphinxdoc
  * switch to source format 3.0 (quilt)

 -- Jan Dittberner <jandd@debian.org>  Sat, 04 Feb 2012 13:50:37 +0100

python-repoze.tm2 (1.0a4-3) unstable; urgency=low

  * Orphan package.

 -- Stefano Zacchiroli <zack@debian.org>  Wed, 01 Feb 2012 18:20:22 +0100

python-repoze.tm2 (1.0a4-2) unstable; urgency=low

  * first upload to unstable
  * debian/control:
    - add build-dep on python-transaction (needed at build time), to
      inhibit easy_install to download it
    - bump Standards-Version to 3.8.2 (no changes needed)

 -- Stefano Zacchiroli <zack@debian.org>  Sun, 02 Aug 2009 13:13:38 +0200

python-repoze.tm2 (1.0a4-1) experimental; urgency=low

  * First release (Closes: #531046)

 -- Stefano Zacchiroli <zack@debian.org>  Fri, 29 May 2009 21:59:03 +0200
